package grafos_test;

import static org.junit.Assert.*;

import org.junit.Test;

import clases_auxiliares.GrafoAleatorio;
import grafos.Grafo;

public class GrafoAleatorio_test {

	@Test
	public void testAleatorios()
	{
		Grafo grafo = GrafoAleatorio.generar(7, 5);
		System.out.println(grafo.getAristas().toString());
		assertEquals(grafo.tama�o(), 7);
		assertEquals(grafo.getAristas().size(), 5);		
	}
	
	@Test
	public void testAleatorios1Vertice()
	{
		Grafo grafo = GrafoAleatorio.generar(1, 0);
		assertEquals(grafo.tama�o(), 1);
		assertEquals(grafo.getAristas().size(), 0);		
	}
	
	@Test
	public void testAleatorios2Vertices()
	{
		Grafo grafo = GrafoAleatorio.generar(2, 1);
		assertEquals(grafo.tama�o(), 2);
		assertEquals(grafo.getAristas().size(), 1);		
	}
	
	@Test
	public void testAleatorios0Vertices()
	{
		Grafo grafo = GrafoAleatorio.generar(0, 0);
		assertEquals(grafo.tama�o(), 0);
		assertEquals(grafo.getAristas().size(), 0);		
	}
	
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void testAleatoriosAristasExcedidas() 
	{
		Grafo grafo = GrafoAleatorio.generar(5, 11);		
	}
	
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void testAleatoriosVerticesNegativos() 
	{
		Grafo grafo = GrafoAleatorio.generar(-5, 0);		
	}
	
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void testAleatoriosAristasNegativas() 
	{
		Grafo grafo = GrafoAleatorio.generar(10, -1);		
	}

	@Test
	public void testAleatoriosGrafoCompleto()
	{
		Grafo grafo = GrafoAleatorio.generar(7, 21);
		assertEquals(grafo.tama�o(), 7);
		assertEquals(grafo.getAristas().size(), 21);		
	}
	
	@SuppressWarnings("unused")
	@Test (expected=IllegalArgumentException.class)
	public void testAleatoriosGrafoCompletoExcedido()
	{
		Grafo grafo = GrafoAleatorio.generar(7, 22);
		
	}
}