package grafos_test;

import static org.junit.Assert.*;

import org.junit.Test;

import grafos.Grafo;

public class Vecinos_test 
{
	
	@Test(expected = IllegalArgumentException.class)
	public void vecinosDeVerticeInexistente()
	{
		Grafo grafo = new Grafo(3);
		grafo.vecinosDe(3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void vecinosDeVerticeInexistente1()
	{
		Grafo grafo = new Grafo(3);
		grafo.vecinosDe(-1);
	}
	
	@Test
	public void VecinosDetest() {
		Grafo grafo = new Grafo(3);
		grafo.agregarArista(0, 1, 0);
		
		int[] esperado = {0};
		assertEquals(1, grafo.vecinosDe(0).size());
		Asserts.iguales(esperado, grafo.vecinosDe(1));
	}
	
	@Test
	public void todosAisladosTest()
	{
		Grafo grafo = new Grafo(3);
		
		int esperado[] = {};
		
		for(int i = 0; i < grafo.tama�o(); i++ )
		{
			assertEquals(0, grafo.vecinosDe(i).size());
			Asserts.iguales(esperado, grafo.vecinosDe(i));
		}
	}
	
	@Test
	public void verticeUniversalTest() 
	{
		Grafo grafo = new Grafo (4);
		grafo.agregarArista(1, 0, 0);
		grafo.agregarArista(1, 2, 0);
		grafo.agregarArista(1, 3, 0);
		
		int[] esperado = {0, 2, 3};
		Asserts.iguales(esperado, grafo.vecinosDe(1));
	}
	
	@Test
	public void verticeNormalTest() 
	{
		Grafo grafo = new Grafo (5);
		grafo.agregarArista(1, 3, 0);
		grafo.agregarArista(2, 3, 0);
		grafo.agregarArista(2, 4, 0);
		
		int[] esperado = {1, 2};
		Asserts.iguales(esperado, grafo.vecinosDe(3));
	}

}
