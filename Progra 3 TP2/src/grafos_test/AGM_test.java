package grafos_test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

import clases_auxiliares.Arista;
import grafos.AGM_BFS;
import grafos.AGM_unionfind;
import grafos.AGM_unionfind_optimizado;
import grafos.Grafo;

public class AGM_test {


	// Ejemplo dado en el PDF de Arbol Generador Minimo del moodle
	@Test
	public void arbolGeneradorTamaņoTest() 
	{
		Grafo grafo = new Grafo(9);
		grafo.agregarArista(0, 1, 4);
		grafo.agregarArista(1, 2, 8);
		grafo.agregarArista(2, 3, 6);
		grafo.agregarArista(3, 4, 9);
		grafo.agregarArista(4, 5, 10);
		grafo.agregarArista(5, 6, 3);
		grafo.agregarArista(6, 7, 1);
		grafo.agregarArista(7, 8, 6);
		grafo.agregarArista(0, 7, 8);
		grafo.agregarArista(1, 7, 12);
		grafo.agregarArista(2, 5, 4);
		grafo.agregarArista(3, 5, 13);
		grafo.agregarArista(2, 8, 3);
		grafo.agregarArista(6, 8, 5);
		
		Grafo arbol1 = AGM_unionfind.algoritmoDeKruskal(grafo);
		Grafo arbol2 = AGM_unionfind_optimizado.algoritmoDeKruskal(grafo);
		Grafo arbol3 = AGM_BFS.algoritmoDeKruskal(grafo);
		
		//En un arbol la cantidad de aristas es igual a la cantidad de vertices -1.
		assertEquals (arbol1.getAristas().size(), grafo.tamaņo() - 1);
		assertEquals (arbol2.getAristas().size(), grafo.tamaņo() - 1);
		assertEquals (arbol3.getAristas().size(), grafo.tamaņo() - 1);
	}

	@Test
	public void arbolGeneradorPesoTest() 
	{
		Grafo grafo = new Grafo(9);
		grafo.agregarArista(0, 1, 4);
		grafo.agregarArista(1, 2, 8);
		grafo.agregarArista(2, 3, 6);
		grafo.agregarArista(3, 4, 9);
		grafo.agregarArista(4, 5, 10);
		grafo.agregarArista(5, 6, 3);
		grafo.agregarArista(6, 7, 1);
		grafo.agregarArista(7, 8, 6);
		grafo.agregarArista(0, 7, 8);
		grafo.agregarArista(1, 7, 12);
		grafo.agregarArista(2, 5, 4);
		grafo.agregarArista(3, 5, 13);
		grafo.agregarArista(2, 8, 3);
		grafo.agregarArista(6, 8, 5);
		
		Grafo arbol1 = AGM_unionfind.algoritmoDeKruskal(grafo);
		Grafo arbol2 = AGM_unionfind_optimizado.algoritmoDeKruskal(grafo);
		Grafo arbol3 = AGM_BFS.algoritmoDeKruskal(grafo);
		
		//En un arbol la cantidad de aristas es igual a la cantidad de vertices -1.
		assertTrue (arbol1.pesoTotal() == 38);
		assertTrue (arbol2.pesoTotal() == 38);
		assertTrue (arbol3.pesoTotal() == 38);
	}
	
	@Test
	public void arbolGeneradorSobreGrafoCompletoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1, 13);
		grafo.agregarArista(0, 2, 21);
		grafo.agregarArista(0, 3, 3); 
		grafo.agregarArista(0, 4, 53);
		grafo.agregarArista(1, 2, 6);
		grafo.agregarArista(1, 3, 30);
		grafo.agregarArista(1, 4, 1);
		grafo.agregarArista(2, 3, 1);
		grafo.agregarArista(2, 4, 1);
		grafo.agregarArista(3, 4, 1);
		
		Grafo arbol = AGM_unionfind_optimizado.algoritmoDeKruskal(grafo);
		
		for(Arista arista : arbol.getAristas())
			System.out.println(arista);
		
		assertEquals (arbol.getAristas().size(), grafo.tamaņo() - 1);
	}
	
	//Si me pasan un grafo no conexo, no puedo armar el arbol generador minimo.
	@Test (expected = IndexOutOfBoundsException.class)
	public void arbolGeneradorSobreGrafoNoConexo()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1, 3);
		grafo.agregarArista(0, 2, 4);
		grafo.agregarArista(1, 2, 22);
		grafo.agregarArista(3, 4, 12);
		
		AGM_unionfind.algoritmoDeKruskal(grafo);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void arbolGeneradorSobreGrafoNull()
	{
		Grafo grafo = null;
		AGM_unionfind.algoritmoDeKruskal(grafo);
	}
	
	@Test
	public void ArbolGeneradorSobreGrafoVacio()
	{
		Grafo grafo = new Grafo (0);
		AGM_unionfind.algoritmoDeKruskal(grafo);
		AGM_unionfind_optimizado.algoritmoDeKruskal(grafo);
		AGM_BFS.algoritmoDeKruskal(grafo);
	}
	
	@Test
	public void ArbolGeneradorSobreGrafoTamaņo1()
	{
		Grafo grafo = new Grafo (1);
		AGM_unionfind.algoritmoDeKruskal(grafo);
		AGM_unionfind_optimizado.algoritmoDeKruskal(grafo);
		AGM_BFS.algoritmoDeKruskal(grafo);
	}
	
	@Test 
	public void ArbolGeneradorSobreGrafoTamaņo2()
	{
		Grafo grafo = new Grafo (2);
		grafo.agregarArista(0, 1, 1);
		
		Grafo arbol1 = AGM_unionfind.algoritmoDeKruskal(grafo);
		Grafo arbol2 = AGM_unionfind_optimizado.algoritmoDeKruskal(grafo);
		Grafo arbol3 = AGM_BFS.algoritmoDeKruskal(grafo);
		
		assertEquals (arbol1.getAristas().size(), grafo.tamaņo()-1);
		assertEquals (arbol2.getAristas().size(), grafo.tamaņo()-1);
		assertEquals (arbol3.getAristas().size(), grafo.tamaņo()-1);
	}
	
	@Test
	public void ordenamientoDeAristasTest()
	{
		ArrayList<Arista> lista = new ArrayList<Arista>();
		Arista arista1 = new Arista(1, 2, 4);
		lista.add(arista1);
		Arista arista2 = new Arista(1, 3, 2);
		lista.add(arista2);
		Arista arista3 = new Arista(1, 4, 5);
		lista.add(arista3);
		Arista arista4 = new Arista(1, 5, 3);
		lista.add(arista4);
		Arista arista5 = new Arista(1, 6, 1);
		lista.add(arista5);
		Collections.sort(lista);
		
		ArrayList<Arista> lista2 = new ArrayList<Arista>();
		lista2.add(arista5);
		lista2.add(arista2);
		lista2.add(arista4);
		lista2.add(arista1);
		lista2.add(arista3);
		
		assertEquals(lista, lista2);

	}
}
