package grafos_test;

import static org.junit.Assert.*;

import org.junit.Test;

import grafos.Grafo;

public class Grafos_test {

	//Testeo de agregado y borrado de v�rtices.
	
	@Test
	public void tama�oTest()
	{
		Grafo grafo = new Grafo(10);
		
		assertEquals(10, grafo.tama�o());
		assertNotEquals(5, grafo.tama�o());
	}
	
	@Test
	public void agregarVerticeTest() 
	{
		Grafo grafo = new Grafo(1);
		grafo.agregarVertice();
		
		assertEquals(2, grafo.tama�o());
	}
	
	@Test
	public void eliminarVerticeTest()
	{
		Grafo grafo = new Grafo(2);
		grafo.eliminarVertice(0);
		
		assertEquals(1, grafo.tama�o());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void eliminarVerticeInexistente()
	{
		Grafo grafo = new Grafo(2);
		grafo.eliminarVertice(2);
	}
	
	//Testeo de agregado y borrado de aristas.
	
	@Test
	public void aristaInexistenteTest() 
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 0);
		assertFalse( grafo.existeArista(1, 4) );
	}
	
	@Test
	public void agregarAristaTest()
	{
		Grafo grafo = new Grafo(6);
		grafo.agregarArista(4, 5, 0);
		
		assertTrue(grafo.existeArista(4, 5));
		assertEquals(1, grafo.getAristas().size());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaVerticesExcedidosTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(4, 5, 0);		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaVerticesNegativosTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(-1, 3, 0);		
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarLoopTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 2, 0);
	}
	
	@Test
	public void agregarAristaDosVecesTest() 
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 0);
		grafo.agregarArista(2, 3, 0);
		assertTrue( grafo.existeArista(2, 3) );
	}
	
	@Test
	public void aristaOpuestaTest() 
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 0);
		assertTrue( grafo.existeArista(3, 2) );
	}
	
	@Test
	public void eliminarAristaTest() 
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3, 0);
		grafo.eliminarArista(2, 3);
		assertFalse( grafo.existeArista(2, 3) );
	}
	
	@Test
	public void eliminarAristaInexistenteTest() 
	{
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 3);
		assertFalse( grafo.existeArista(2, 3) );
	}
	
	@Test
	public void eliminarAristaDosVecesTest() 
	{
		Grafo grafo = new Grafo(5);
		grafo.eliminarArista(2, 3);
		grafo.eliminarArista(2, 3);
		assertFalse( grafo.existeArista(2, 3) );
	}
}
