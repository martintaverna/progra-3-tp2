package grafos_test;

import static org.junit.Assert.*;

import org.junit.Test;

import clases_auxiliares.Arista;
import grafos.Grafo;

public class AristasConPeso_test {

	//Dos aristas son iguales si sus dos vertices son iguales.
	@Test
	public void equalsConDistintoPesoTest()
	{
		Arista arista1 = new Arista(1, 2, 50);
		Arista arista2 = new Arista(1, 2, 0);
		
		assertEquals(arista1, arista2);
	}
	
	@Test
	public void equalsVerticesInvertidos()
	{
		Arista arista1 = new Arista(1, 2, 0);
		Arista arista2 = new Arista(2, 1, 0);
		
		assertEquals(arista1, arista2);
	}

	@Test
	public void consultarPesoTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		
		assertTrue(5 == grafo.pesoArista(0, 2));
	}
	
	@Test
	public void consultarInvertidoPesoTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		
		assertTrue(5 == grafo.pesoArista(2, 0));
	}
	
	@Test
	public void consultarPesoErroneoTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 6);
		
		assertNotEquals(5, grafo.pesoArista(0, 2));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void consultarPesoAristaInexistenteTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		
		grafo.pesoArista(0, 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void eliminarAristaInversaTest() 
	{
		Grafo grafo = new Grafo (5);
		
		grafo.agregarArista(0, 2, 5);
		grafo.eliminarArista(2, 0);
		
		grafo.pesoArista(0, 2);
	}
	
	@Test
	public void comparableTest()
	{
		Arista arista1 = new Arista (1, 2, 20);
		Arista arista2 = new Arista (1, 2, 50);
		Arista arista3 = new Arista (1, 2, 20);
		
		assertTrue(arista1.compareTo(arista2) == -1);
		assertTrue(arista1.compareTo(arista3) == 0);
		assertTrue(arista2.compareTo(arista1) == 1);
	}
	
}
