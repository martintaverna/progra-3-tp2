package clases_auxiliares;

import java.util.Objects;

public class Tupla<T1, T2> 
{
	private T1 a;
	private T2 b;
	
	public void set (T1 a, T2 b)
	{
		this.a = a;
		this.b = b;
	}
	
	public T1 get1()
	{
		return this.a;
	}
	public T2 get2()
	{
		return this.b;
	}

	@Override
	public int hashCode() {
		return Objects.hash(a, b);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tupla<?, ?> other = (Tupla<?, ?>) obj;
		return Objects.equals(a, other.a) && Objects.equals(b, other.b);
	}


}