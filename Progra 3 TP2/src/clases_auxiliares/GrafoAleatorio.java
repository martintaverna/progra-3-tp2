package clases_auxiliares;

import java.util.ArrayList;
import java.util.Random;

import grafos.Grafo;


public class GrafoAleatorio 
{

	public static Grafo generar (int vertices, int aristas)
	{
		Grafo ret;
		if (vertices > 750)
			ret = generarGrafoAproximado(vertices, aristas);
		else
			ret =generarGrafoExacto(vertices, aristas);
		return ret;
	}
	
	private static Grafo generarGrafoExacto(int vertices, int aristas)
	{
		chequearValidez(vertices, aristas);
		
		Grafo ret = new Grafo(vertices);
		ArrayList<Tupla<Integer, Integer>> lista_aristas = rellenarTuplas(vertices);
		Random rand = new Random();
		int i = 0;
		while (i < aristas)
		{
			int aux = rand.nextInt(lista_aristas.size());
			float peso = rand.nextFloat();
			
			Tupla<Integer, Integer> tupla_aux = lista_aristas.get(aux);
			ret.agregarArista(tupla_aux.get1(), tupla_aux.get2(), peso);
			lista_aristas.remove(aux);
			
			i++;
		}
		return ret;
	}
	
	private static ArrayList<Tupla<Integer, Integer>> rellenarTuplas(int vertices)
	{
		ArrayList<Tupla<Integer, Integer>> ret = new ArrayList<Tupla<Integer, Integer>>();
		int i = 0;
		while (i < vertices - 1)
		{
			for (int j = i+1; j < vertices; j++)
			{
				Tupla<Integer,Integer> aux = new Tupla<Integer,Integer>();
				aux.set(i, j);
				ret.add(aux);
			}
			i++;
		}	
		return ret;
	}
	
	
	private static Grafo generarGrafoAproximado(int vertices, int aristas)
	{
		chequearValidez(vertices, aristas);
		Grafo ret = new Grafo(vertices);
		
		Random rand = new Random();
		int i = 0;
		while(i < aristas)
		{
			int x = rand.nextInt(vertices);
			int y = rand.nextInt(vertices);
			float z = rand.nextFloat();
			
			if (x != y)
				ret.agregarArista(x, y, z);
			i++;
		}
		return ret;
	}
	private static void chequearValidez(int vertices, int aristas) {
		if (vertices < 0 || aristas < 0)
			throw new IllegalArgumentException("Solo recibo numero positivos.");
		
		if(aristas > (vertices*(vertices-1))/2)
			throw new IllegalArgumentException("La m�xima cantidad de aristas es (n*n-1)/2");
	}
}