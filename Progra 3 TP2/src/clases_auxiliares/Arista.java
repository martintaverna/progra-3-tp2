package clases_auxiliares;

public class Arista implements Comparable<Arista> 
{

	// Representamos a las aristas con sus dos vertices y su peso.
	private int ver1;
	private int ver2;
	private float peso;
	
	
	// Constructor de la clase.
	public Arista (int vertice1, int vertice2, float peso)
	{
		this.ver1 = vertice1;
		this.ver2 = vertice2;
		this.peso = peso;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ver1;
		result = prime * result + ver2;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arista other = (Arista) obj;
		if (ver1 != other.ver1 && ver1 != other.ver2)
			return false;
		if (ver2 != other.ver2 && ver2 != other.ver1)
			return false;
		return true;
	}
	
	// toString, devuelve un String del tipo "(vertice1, vertice2, peso)"
	public String toString() 
	{
		String ret = "(" + ver1 + ", " + ver2 + ", " + peso + ")";
		return ret;
	}
	
	// Getters y setters.
	public int getVer1() {
		return ver1;
	}

	public int getVer2() {
		return ver2;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}


	@Override
	public int compareTo(Arista other) {
		if (this.getPeso() > other.getPeso())
			return 1;
		else if (this.getPeso() < other.getPeso())
			return -1;
		else
			return 0;
	}	
}
