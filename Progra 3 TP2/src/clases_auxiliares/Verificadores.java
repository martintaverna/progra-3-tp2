package clases_auxiliares;

import grafos.Grafo;

public class Verificadores 
{
	// Verifica que no se introduzcan loops
	public static void verificarDistintos(int i, int j) {
			if( i == j)
				throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")." );
	}
	
	// Verifica que sea un vertice valido
	public static void verificarVertices(int i, int j, Grafo grafo) {
			if( i < 0 || j < 0)
				throw new IllegalArgumentException("El vertice no puede ser negativo: " + i + ".");
			
			if( i >= grafo.tamaño() || j >= grafo.tamaño())
				throw new IllegalArgumentException("Los vertices deben estar entre 0 y |v|-1: " + i + ".");
	}
	
	// Verifica que la cantidad de vertices sea >= 0
	public static void verificarTamañoNegativo (int i)
	{
		if (i < 0)
			throw new IllegalArgumentException("Se esta intentando crear un grafo con !V! < 0: " + i + ".");
	}
	
	// Este método verifica que no se haya ingresado un grafo null.
		public static void verificarNull(Grafo grafo)
		{
			if (grafo == null)
				throw new IllegalArgumentException("Se intento consultar un grafo no inicializado");
		}	
	
	
}
