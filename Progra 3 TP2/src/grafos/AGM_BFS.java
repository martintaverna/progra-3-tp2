package grafos;

import java.util.ArrayList;
import java.util.Collections;

import clases_auxiliares.Arista;
import clases_auxiliares.Verificadores;

public class AGM_BFS 
{

	private static ArrayList<Arista> lista_aristas;
	
	private static void inicializar(Grafo grafo)
	{
		lista_aristas = new ArrayList<Arista>();
		
		for (Arista arista : grafo.getAristas())
			lista_aristas.add(arista);
		
		Collections.sort(lista_aristas);		
	}

	static public Grafo algoritmoDeKruskal(Grafo grafo)
	{
		Verificadores.verificarNull(grafo);
		
		Grafo AGM = new Grafo(grafo.tama�o());
		inicializar(grafo);
		
		int i = 0;
		while (AGM.getAristas().size() < grafo.tama�o() - 1)
		{
			Arista arista = lista_aristas.get(i);
			
			//Si el "Vertice1" NO contiene dentro de sus alcanzables a "Vertice2" entonces agrego la arista.
			if (!BFS.alcanzables(AGM, arista.getVer1()).contains(arista.getVer2()))	
			{	
				AGM.agregarArista(arista.getVer1(), arista.getVer2(), arista.getPeso());
			}
			i++;
		}
		return AGM;
	}
}
