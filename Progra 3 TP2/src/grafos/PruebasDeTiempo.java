package grafos;

import clases_auxiliares.GrafoAleatorio;

public class PruebasDeTiempo {

	public static void main(String[] args) 
	{
		long TInicio, TFin, tiempoUF = 0;
		
		Grafo grafo = obtenerGrafoConexo(10, 30);
		
		TInicio = System.currentTimeMillis();
		for (int m = 0; m < 1; m++)
		{
			AGM_BFS.algoritmoDeKruskal(grafo);
		}
		TFin = System.currentTimeMillis();
		
		tiempoUF = (TFin - TInicio);
		
		System.out.println("Tiempo de ejecución de unionfind en milisegundos: " + tiempoUF);
	}
	
	static Grafo obtenerGrafoConexo(int vertices, int aristas)
	{
		Grafo ret = GrafoAleatorio.generar(vertices, aristas);
		
		while (!BFS.esConexo(ret))
		{
			ret = GrafoAleatorio.generar(vertices, aristas);
		}
		return ret;
	}
}
