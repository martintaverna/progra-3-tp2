package grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import clases_auxiliares.Arista;
import clases_auxiliares.Verificadores;

public class Grafo 
{

	// Representamos el grafo mediante lista de vecinos
	// Guardamos todas las aristas en una lista para almacenar informacion como el peso
	private ArrayList<HashSet<Integer>> vertices;
	private ArrayList<Arista> aristas;

	
	// La cantidad de vertices esta predeterminada desde el constructor
	public Grafo (int vertices)
	{
		Verificadores.verificarTamaņoNegativo(vertices);
		
		this.vertices = new ArrayList<HashSet<Integer>>();
		for ( int i = 0; i < vertices; i++)
			this.vertices.add(new HashSet<Integer>());
		
		this.aristas = new ArrayList<Arista>();

	}

	// Agregado de vertices
	public void agregarVertice()
	{
		this.vertices.add(new HashSet<Integer>());
	}

	// Metodo que recibe la posicion de un vertice y lo elimina 
	public void eliminarVertice(int i)
	{	
		Verificadores.verificarVertices(i, i, this);
		
		for (int j = 0; j < this.tamaņo(); j++)
			if (this.vertices.get(j).contains(i))
				this.eliminarArista(i, j);
		
		this.vertices.remove(i);
	}
	
	// Agregado de aristas con peso definido
	public void agregarArista (int i, int j, float peso)
	{
		Verificadores.verificarVertices(i, j, this);
		Verificadores.verificarDistintos(i, j);
		
		this.vertices.get(i).add(j);
		this.vertices.get(j).add(i);
		
		this.aristas.add(new Arista(i, j, peso));	
	}

	// Eliminado de aristas
	public void eliminarArista (int i, int j)
		{
			Verificadores.verificarVertices(i, j, this);
			Verificadores.verificarDistintos(i, j);
			
			if (this.existeArista(i, j))
			{
				this.vertices.get(i).remove(j);
				this.vertices.get(j).remove(i);
				
				Arista aux = new Arista(i, j, 0);
				this.aristas.remove(aux);
			}
		}

	// Informo si existe la arista especificada
	public boolean existeArista (int i, int j)
	{
		Verificadores.verificarVertices(i, j, this);
		
		return this.vertices.get(i).contains(j);
	}
	
	// Devuelvo el peso (o coste) de la arista que conecta a los vertices i y j.
	public float pesoArista (int i, int j)
	{
		Verificadores.verificarVertices(i, j, this);
		
		if (this.existeArista(i, j))
		{
			Arista aux = new Arista(i, j, 0);
			
			for (Arista arista : this.aristas)
				if (arista.equals(aux))
					return arista.getPeso();
		}
		throw new IllegalArgumentException("La arista (" + i + ", " + j + ") no existe.");
	}

	// Devuelvo la lista que contiene a todos los vecinos del vertice especificado
	public Set<Integer> vecinosDe (int i)
	{
		Verificadores.verificarVertices(i, i, this);
		
		return this.vertices.get(i);
	}
	
	// Cantidad de vertices
	public int tamaņo ()
	{
		return vertices.size();
	}
	
	// Cantidad de vertices
	public int cantAristas ()
	{
		return this.aristas.size();
	}

	// La suma de los pesos de todas las aristas pertenecientes al grafo.
	public float pesoTotal()
	{
		float ret = 0;
		for (Arista arista : this.aristas)
			ret += arista.getPeso();
		return ret;
	}

	public ArrayList<Arista> getAristas()
	{
		return this.aristas;
	}
	
	public ArrayList<HashSet<Integer>> getVecinos()
	{
		return this.vertices;
	}
}
