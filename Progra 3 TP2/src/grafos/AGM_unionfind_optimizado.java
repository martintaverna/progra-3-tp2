package grafos;

import java.util.ArrayList;
import java.util.Collections;

import clases_auxiliares.Arista;
import clases_auxiliares.Verificadores;

public class AGM_unionfind_optimizado 
{

	static int [] padre;
	static int [] peso;
	static ArrayList<Arista> lista;

	// Inicializo las listas auxiliares del metodo algoritmoDeKruslal.
	private static void inicializar(Grafo grafo)
	{
		lista = new ArrayList<Arista>();
		for (Arista arista : grafo.getAristas())
			lista.add(arista);
		Collections.sort(lista);
		
		padre = new int[grafo.tama�o()];
		peso = new int[grafo.tama�o()];		
		for (int i = 0; i < grafo.tama�o(); i++)
		{
			padre[i] = i;
			peso[i] = 1;
		}				
	}
	
	// Este metodo devuelve un AGM del grafo recibido.
	static public Grafo algoritmoDeKruskal(Grafo grafo)
	{
		Verificadores.verificarNull(grafo);

		Grafo AGM = new Grafo(grafo.tama�o());
		inicializar(grafo);
		
		int i = 0;		
		while(AGM.getAristas().size() < grafo.tama�o() - 1)
		{
			Arista arista = lista.get(i);
			if (!find(arista.getVer1(), arista.getVer2()))
			{
				AGM.agregarArista(arista.getVer1(),arista.getVer2(), arista.getPeso());
				union(arista.getVer1(), arista.getVer2());
			}
			i++;
		}
		return AGM;		
	}
	
	// Devuelve la raiz del arbol al que pertenece el vertice especificado
	private static int root (int i)
	{
		if (i != padre[i])
			padre[i] = root(padre[i]);
		return padre[i];
	}
	
	// Devuelve si los dos vertices especificados pertenecen a la misma componente conexa
	private static boolean find (int i, int j)
	{
		return root(i) == root(j);
	}
	
	// Une dos componentes conexas distintas
	private static void union (int i, int j)
	{
		int ri = root(i);
		int rj = root(j);
		
		if(peso[ri] < peso[rj])
		{
			padre[ri] = rj;
			peso[rj] += peso[ri];
		}
		else 
		{
			padre[rj] = ri;
			peso[ri] += peso[rj];
		}	
	}	
}
